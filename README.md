CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Use case

INTRODUCTION
------------
Provides ability for any user to add complains about any content entity.

REQUIREMENTS
------------
* Filter (core)
* User (core)

RECOMMENDED MODULES
-------------------
* Token https://www.drupal.org/project/token
* Allowed Formats https://www.drupal.org/project/allowed_formats

INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
Visit https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

CONFIGURATION
-------------
* Visit `/admin/structure/entity-abuse` page and set up:
  * On **Enabled** section choose all content types which should have
  ability to add complaint.
  * On **Report link behavior** section choose an action which should
  be done on click by "Add complaint" link.
  * On **When cancelling a user account** section choose what to do with
  reports added by user on user cancelling.
  * On **No access behavior** section choose what to do when user isn't
  allowed to add a complaint.
  * On **Report adding** tab:
    * **Label for: Add report button** - text for "Add complaint" link.
    * **Message: Report was added** - message to display once complaint
    was added.
    * **Message: You have no access** - message to display if user isn't
      allowed to add a complaint (appears only when **No access behavior**
      = Show report link).
  * On **Report editing** tab:
    * **Label for: Edit report button** - text for "Edit complaint" link.
    * **Message: Report was updated** - message to display once complaint
      was edited.
  * On **Report canceling** tab:
    * **Label for: Cancel report button** - text for "Cancel complaint" link.
    * **Message: Report was canceled** - message to display once complaint
      was cancelled.
    * **Notification message on report canceling** - Complaint cancelling
    confirmation message.
  * Press **Save configuration** button.
* To have ability to translate text strings entered in above instructions:
  * Enable **Configuration Translation** core module
  * On module configuration page `/admin/structure/entity-abuse`
    new tab **Translate entity abuse** will appear
  * Use this tab to add translations
* Visit **Manage fields** `/admin/structure/entity-abuse/fields` tab
  to set up fields which complaint should have.
  * By default, there is a single field **Message** entity_abuse_report
  with format: Text (formatted, long).
* Visit **Manage form display** `/admin/structure/entity-abuse/form-display`
  tab to set up complaint form display.
  * By default, **Message** field displayed as: Text area (multiple rows).
* Visit all content types (chosen on `/admin/structure/entity-abuse` page
  **Enabled** section) -> tab **Manage display** -> needed display mode to
  set up **Abuse report link** field display.
* Set up permissions on `/admin/people/permissions#module-entity_abuse` page
  * If user have permission to edit own (or any) Abuse report - Edit link
  will be displayed after Abuse report adding.
  * If user have permission to delete own (or any) Abuse report - Delete link
  will be displayed after Abuse report adding.
