<?php

namespace Drupal\entity_abuse;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;

/**
 * Entity abuse handling helper service.
 */
class EntityAbuseService implements EntityAbuseServiceInterface {

  /**
   * The aggregator.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The discovery and retrieval of entity type bundles.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current active database's master connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new EntityAbuseService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The discovery and retrieval of entity type bundles.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The current active database's master connection.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $connection
  ) {
    $this->config = $config_factory->get('entity_abuse.settings');
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledEntityTypes() {
    $result = [];

    $entity_types = $this->config->get('enabled');
    foreach ($entity_types as $entity_type_id) {
      $bundle_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      $result[$entity_type_id] = array_keys($bundle_info);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function isReportLinkDialog() {
    return in_array($this->getReportLinkBehavior(), ['dialog', 'modal']);
  }

  /**
   * {@inheritdoc}
   */
  public function getReportLinkBehavior() {
    return (string) $this->config->get('report_link_behavior');
  }

  /**
   * {@inheritdoc}
   */
  public function getUserCancelMethod() {
    return (string) $this->config->get('user_cancel_method');
  }

  /**
   * {@inheritdoc}
   */
  public function getNoAccessBehavior() {
    return (string) $this->config->get('no_access_behavior');
  }

  /**
   * {@inheritdoc}
   */
  public function getAddReportLinkLabel() {
    return (string) $this->config->get('label_add_report');
  }

  /**
   * {@inheritdoc}
   */
  public function getReportAddedMessage() {
    return $this->config->get('message_report_added');
  }

  /**
   * {@inheritdoc}
   */
  public function getNoAccessMessage() {
    return $this->config->get('message_no_access');
  }

  /**
   * {@inheritdoc}
   */
  public function getEditReportLinkLabel() {
    return (string) $this->config->get('label_edit_report');
  }

  /**
   * {@inheritdoc}
   */
  public function getReportEditedMessage() {
    return $this->config->get('message_report_edited');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelReportLinkLabel() {
    return (string) $this->config->get('label_cancel_report');
  }

  /**
   * {@inheritdoc}
   */
  public function getReportCanceledMessage() {
    return $this->config->get('message_report_canceled');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelReportNotification() {
    return $this->config->get('note_cancel_report');
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingReport($entity_id, $entity_type) {
    $result = $this->entityTypeManager
      ->getStorage('entity_abuse_report')
      ->loadByProperties([
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'uid' => $this->currentUser->id(),
      ]);

    return $result ? reset($result) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function userDelete(UserInterface $user) {
    $method = $this->getUserCancelMethod();

    if ($method === 'reassign') {
      $this->connection
        ->update('entity_abuse_report')
        ->fields(['uid' => 0])
        ->condition('uid', $user->id(), '=')
        ->execute();
    }
    elseif ($method === 'delete') {
      $this->connection
        ->delete('entity_abuse_report')
        ->condition('uid', $user->id(), '=')
        ->execute();
    }
  }

}
