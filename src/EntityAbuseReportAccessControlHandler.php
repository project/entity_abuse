<?php

namespace Drupal\entity_abuse;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the entity_abuse_report entity.
 */
class EntityAbuseReportAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_abuse\EntityAbuseReportInterface $entity */

    if (($admin_permission = $this->entityType->getAdminPermission()) && $account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    switch ($operation) {
      case 'view':
        if ($account->hasPermission('view any entity_abuse_report')) {
          return AccessResult::allowed()
            ->cachePerPermissions()
            ->addCacheableDependency($entity);
        }
        if ($account->hasPermission('view own entity_abuse_report') && $entity->getOwnerId() == $account->id()) {
          return AccessResult::allowed()
            ->cachePerPermissions()
            ->cachePerUser()
            ->addCacheableDependency($entity);
        }
        break;

      case 'update':
        if ($account->isAuthenticated()) {
          if ($account->hasPermission('edit any entity_abuse_report')) {
            return AccessResult::allowed()
              ->cachePerPermissions()
              ->cachePerUser()
              ->addCacheableDependency($entity);
          }
          if ($account->hasPermission('edit own entity_abuse_report') && $entity->getOwnerId() == $account->id()) {
            return AccessResult::allowed()
              ->cachePerPermissions()
              ->cachePerUser()
              ->addCacheableDependency($entity);
          }
        }
        break;

      case 'delete':
        if ($account->isAuthenticated()) {
          if ($account->hasPermission('delete any entity_abuse_report')) {
            return AccessResult::allowed()
              ->cachePerPermissions()
              ->cachePerUser()
              ->addCacheableDependency($entity);
          }
          if ($account->hasPermission('delete own entity_abuse_report') && $entity->getOwnerId() == $account->id()) {
            return AccessResult::allowed()
              ->cachePerPermissions()
              ->cachePerUser()
              ->addCacheableDependency($entity);
          }
        }
    }

    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    if (($admin_permission = $this->entityType->getAdminPermission()) && $account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    return AccessResult::allowedIfHasPermission($account, 'add entity_abuse_report');
  }

}
