<?php

namespace Drupal\entity_abuse\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_abuse\EntityAbuseServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a deletion confirmation form for entity_abuse_report.
 */
class EntityAbuseReportDeleteForm extends ContentEntityDeleteForm {

  /**
   * The entity abuse service.
   *
   * @var \Drupal\entity_abuse\EntityAbuseServiceInterface
   */
  protected $entityAbuseService;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\entity_abuse\EntityAbuseServiceInterface $entity_abuse_service
   *   The entity abuse service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, EntityAbuseServiceInterface $entity_abuse_service) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->entityAbuseService = $entity_abuse_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_abuse.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if (($message = $this->entityAbuseService->getCancelReportNotification()) && !empty($message['value'])) {
      return check_markup($message['value'], $message['format']);
    }

    return $this->t('Are you sure you want to delete this complain. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    if (($message = $this->entityAbuseService->getReportCanceledMessage()) && !empty($message['value'])) {
      return check_markup($message['value'], $message['format']);
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Skip');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Cancel your report');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    $message = $this->getDeletionMessage();

    // Make sure that deleting a translation does not delete the whole entity.
    if (!$entity->isDefaultTranslation()) {
      $untranslated_entity = $entity->getUntranslated();
      $untranslated_entity->removeTranslation($entity->language()->getId());
      $untranslated_entity->save();
      $form_state->setRedirectUrl($untranslated_entity->toUrl('canonical'));
    }
    else {
      $entity->delete();
      $form_state->setRedirectUrl($this->getRedirectUrl());
    }

    if ($message) {
      $this->messenger()->addStatus($message);
    }
    $this->logDeletionMessage();
  }

}
