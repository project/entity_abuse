<?php

namespace Drupal\entity_abuse\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_abuse\EntityAbuseServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for handler for entity_abuse_report edit forms.
 */
class EntityAbuseReportForm extends ContentEntityForm {

  /**
   * The entity abuse service.
   *
   * @var \Drupal\entity_abuse\EntityAbuseServiceInterface
   */
  protected $entityAbuseService;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\entity_abuse\EntityAbuseServiceInterface $entity_abuse_service
   *   The entity abuse service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL, EntityAbuseServiceInterface $entity_abuse_service) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->entityAbuseService = $entity_abuse_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_abuse.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['actions']['submit']['#value'] = $this->entity->isNew() ?
      $this->t('Submit') : $this->t('Update');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // Validate entity type and id to prevent complains
    // to not existing or not accessible entities.
    $reported_entity = $this->getReportedEntity();

    if (
      $this->entity->isNew() &&
      (
        !($reported_entity instanceof ContentEntityInterface) ||
        !$reported_entity->access('view')
      )
    ) {
      $form_state->setError($form, $this->t(
        'An illegal choice has been detected. Please contact the site administrator.'
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    if ($this->entity->isNew()) {
      if (($message = $this->entityAbuseService->getReportAddedMessage()) && !empty($message['value'])) {
        $this->messenger()->addStatus(check_markup($message['value'], $message['format']));
      }

      $reported_entity = $this->getReportedEntity();

      if ($reported_entity instanceof ContentEntityInterface) {
        $this->entity->set('entity_type', $reported_entity->getEntityTypeId());
        $this->entity->set('entity_id', $reported_entity->id());
      }
    }
    else {
      if (($message = $this->entityAbuseService->getReportEditedMessage()) && !empty($message['value'])) {
        $this->messenger()->addStatus(check_markup($message['value'], $message['format']));
      }
    }

    parent::save($form, $form_state);

    $form_state->setRedirect(
      'entity.entity_abuse_report.canonical',
      ['entity_abuse_report' => $this->entity->id()]
    );
  }

  /**
   * Return reported entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Reported entity on success or NULL otherwise.
   */
  protected function getReportedEntity() {

    $entity_type = trim((string) $this->getRouteMatch()->getParameter('entity_type'));
    $entity_id = (int) $this->getRouteMatch()->getParameter('entity_id');

    if (!$entity_type || !$entity_id) {
      return NULL;
    }

    $enabled_entity_types = $this->entityAbuseService->getEnabledEntityTypes();

    if (!isset($enabled_entity_types[$entity_type])) {
      return NULL;
    }

    $storage = $this->entityTypeManager->getStorage($entity_type);

    if (!($storage instanceof ContentEntityStorageInterface)) {
      return NULL;
    }

    return $storage->load($entity_id);
  }

}
