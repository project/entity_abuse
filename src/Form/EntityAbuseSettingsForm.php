<?php

namespace Drupal\entity_abuse\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Entity abuse settings.
 */
class EntityAbuseSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_abuse_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_abuse.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('entity_abuse.settings');
    $definitions = $this->entityTypeManager->getDefinitions();

    $content_entity_types = [];
    foreach ($definitions as $definition) {
      if ($definition instanceof ContentEntityTypeInterface && $definition->id() !== 'entity_abuse_report') {

        $content_entity_types[$definition->id()] = $definition->getLabel();
      }
    }

    $form['enabled'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('enabled'),
      '#options' => $content_entity_types,
      '#description' => $this->t('Choose entity types which will have "Report abuse" field.'),
      '#required' => TRUE,
    ];
    $form['report_link_behavior'] = [
      '#type' => 'radios',
      '#default_value' => $config->get('report_link_behavior'),
      '#title' => $this->t('Report link behavior'),
      '#options' => [
        'redirect' => $this->t('Land user to a new page'),
        'dialog' => $this->t('Open dialog'),
        'modal' => $this->t('Open modal dialog'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('Choose what report link will do on click by it.'),
    ];
    $form['user_cancel_method'] = [
      '#type' => 'radios',
      '#default_value' => $config->get('user_cancel_method'),
      '#title' => $this->t('When cancelling a user account'),
      '#options' => [
        'delete' => $this->t('Delete all reports added by user'),
        'reassign' => $this->t('Re-assign all reports of a user to anonymous'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('What to do with reports added by user on cancelling.'),
    ];
    $form['no_access_behavior'] = [
      '#type' => 'radios',
      '#default_value' => $config->get('no_access_behavior'),
      '#title' => $this->t('No access behavior'),
      '#options' => [
        'hide' => $this->t('Hide report link'),
        'message' => $this->t('Show report link: On click display "You have no access" message'),
      ],
      '#required' => TRUE,
      '#description' => $this->t("What to do when user isn't allowed to add new report."),
    ];

    $form['additional_settings'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['report_add'] = [
      '#type' => 'details',
      '#title' => $this->t('Report adding'),
      '#group' => 'additional_settings',
      '#open' => TRUE,
    ];
    $form['report_add']['label_add_report'] = [
      '#title' => $this->t('Label for: Add report button'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $config->get('label_add_report'),
      '#required' => TRUE,
    ];
    $form['report_add']['message_report_added'] = [
      '#title' => $this->t('Message: Report was added'),
      '#description' => $this->t('Leave empty to disable message after report adding.'),
      '#type' => 'text_format',
      '#default_value' => $config->get('message_report_added.value'),
      '#format' => $config->get('message_report_added.format'),
      '#rows' => 2,
    ];
    $form['report_add']['message_no_access'] = [
      '#title' => $this->t('Message: You have no access'),
      '#description' => $this->t("Message to display to user who doesn't have permission to add new report."),
      '#type' => 'text_format',
      '#default_value' => $config->get('message_no_access.value'),
      '#format' => $config->get('message_no_access.format'),
      '#rows' => 2,
      '#states' => [
        'visible' => [
          'input[name="no_access_behavior"]' => ['value' => 'message'],
        ],
        'required' => [
          'input[name="no_access_behavior"]' => ['value' => 'message'],
        ],
      ],
    ];

    $form['report_edit'] = [
      '#type' => 'details',
      '#title' => $this->t('Report editing'),
      '#group' => 'additional_settings',
      '#open' => FALSE,
    ];
    $form['report_edit']['label_edit_report'] = [
      '#title' => $this->t('Label for: Edit report button'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $config->get('label_edit_report'),
      '#required' => TRUE,
    ];
    $form['report_edit']['message_report_edited'] = [
      '#title' => $this->t('Message: Report was updated'),
      '#description' => $this->t('Leave empty to disable message after report update.'),
      '#type' => 'text_format',
      '#default_value' => $config->get('message_report_edited.value'),
      '#format' => $config->get('message_report_edited.format'),
      '#rows' => 2,
    ];

    $form['report_cancel'] = [
      '#type' => 'details',
      '#title' => $this->t('Report canceling'),
      '#group' => 'additional_settings',
      '#open' => FALSE,
    ];
    $form['report_cancel']['label_cancel_report'] = [
      '#title' => $this->t('Label for: Cancel report button'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $config->get('label_cancel_report'),
      '#required' => TRUE,
    ];
    $form['report_cancel']['message_report_canceled'] = [
      '#title' => $this->t('Message: Report was canceled'),
      '#description' => $this->t('Leave empty to disable message after report canceling.'),
      '#type' => 'text_format',
      '#default_value' => $config->get('message_report_canceled.value'),
      '#format' => $config->get('message_report_canceled.format'),
      '#rows' => 2,
    ];
    $form['report_cancel']['note_cancel_report'] = [
      '#title' => $this->t('Notification message on report canceling'),
      '#description' => $this->t('This notification will be displayed to the user on report canceling page (or popup).'),
      '#type' => 'text_format',
      '#default_value' => $config->get('note_cancel_report.value'),
      '#format' => $config->get('note_cancel_report.format'),
      '#rows' => 2,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('entity_abuse.settings')
      ->set('enabled', array_values(array_filter($form_state->getValue('enabled'))))
      ->set('report_link_behavior', $form_state->getValue('report_link_behavior'))
      ->set('user_cancel_method', $form_state->getValue('user_cancel_method'))
      ->set('no_access_behavior', $form_state->getValue('no_access_behavior'))
      ->set('label_add_report', $form_state->getValue('label_add_report'))
      ->set('message_report_added', $form_state->getValue('message_report_added'))
      ->set('message_no_access', $form_state->getValue('message_no_access'))
      ->set('label_edit_report', $form_state->getValue('label_edit_report'))
      ->set('message_report_edited', $form_state->getValue('message_report_edited'))
      ->set('label_cancel_report', $form_state->getValue('label_cancel_report'))
      ->set('message_report_canceled', $form_state->getValue('message_report_canceled'))
      ->set('note_cancel_report', $form_state->getValue('note_cancel_report'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
