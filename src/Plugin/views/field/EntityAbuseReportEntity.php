<?php

namespace Drupal\entity_abuse\Plugin\views\field;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_abuse\EntityAbuseReportInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler for related entity link display.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("entity_abuse_report_entity")
 */
class EntityAbuseReportEntity extends FieldPluginBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a TimeInterval plugin object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $report = property_exists($values, '_entity') ? $values->_entity : NULL;

    if ($report instanceof EntityAbuseReportInterface) {
      $entity_type_id = $report->entity_type->value;
      $entity_id = $report->entity_id->value;

      if ($entity_id && $entity_type_id) {
        $storage = $this->entityTypeManager->getStorage($entity_type_id);

        if ($storage instanceof EntityStorageInterface) {
          $entity = $storage->load($entity_id);

          if ($entity instanceof EntityInterface) {
            return [
              '#type' => 'link',
              '#title' => $entity->label(),
              '#url' => $entity->toUrl(),
            ];
          }
        }
      }
    }

    return [
      '#markup' => $this->t('Undefined entity.'),
    ];
  }

}
