<?php

namespace Drupal\entity_abuse;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Service for build a simple search form.
 */
class EntityAbuseReportLinkLazyBuilder implements TrustedCallbackInterface {

  /**
   * The entity abuse service.
   *
   * @var \Drupal\entity_abuse\EntityAbuseServiceInterface
   */
  protected $entityAbuseService;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new EntityAbuseReportLinkLazyBuilder object.
   *
   * @param \Drupal\entity_abuse\EntityAbuseServiceInterface $entity_abuse_service
   *   The entity abuse service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   */
  public function __construct(EntityAbuseServiceInterface $entity_abuse_service, AccountProxyInterface $current_user) {
    $this->entityAbuseService = $entity_abuse_service;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['getLink'];
  }

  /**
   * Lazy builder callback to build the report link.
   *
   * @param int $entity_id
   *   ID of an entity to build report for.
   * @param string $entity_type
   *   Type ID of an entity to build report for.
   * @param string $destination
   *   Link destination.
   *
   * @return array
   *   A render-able form array.
   */
  public function getLink($entity_id, $entity_type, $destination) {
    $entity_id = (int) $entity_id;
    $entity_type = trim($entity_type);
    $attributes = ['class' => ['ear-link']];

    if ($this->entityAbuseService->isReportLinkDialog()) {
      $attributes['data-dialog-type'] = $this->entityAbuseService->getReportLinkBehavior();
      $attributes['data-dialog-options'] = Json::encode([
        'width' => 'auto',
        'height' => 'auto',
      ]);
      $attributes['class'][] = 'use-ajax';
    }

    $build = [
      '#theme' => 'entity_abuse_report_links',
      '#attached' => [
        'library' => ['core/drupal.dialog.ajax'],
      ],
      '#cache' => [
        'max-age' => 0,
        'tags' => ['config:entity_abuse.settings'],
      ],
    ];

    if (!$this->currentUser->hasPermission('add entity_abuse_report')) {

      if ($this->entityAbuseService->getNoAccessBehavior() === 'hide') {
        return $build;
      }

      $attributes['class'] = array_merge(
        $attributes['class'],
        ['create', 'no-access']
      );
      $attributes['href'] = Url::fromRoute(
        'entity_abuse.no_access',
        ['entity_type' => $entity_type, 'entity_id' => $entity_id]
      )->toString();

      $build['#no_access'] = TRUE;
      $build['#no_access_label'] = $this->entityAbuseService->getAddReportLinkLabel();
      $build['#no_access_attributes'] = new Attribute($attributes);

      return $build;
    }

    $existing_report = $this->entityAbuseService->getExistingReport($entity_id, $entity_type);
    if ($existing_report instanceof EntityAbuseReportInterface) {

      if ($this->currentUser->hasPermission('edit own entity_abuse_report')) {
        $edit_attributes = $attributes;
        $edit_attributes['class'][] = 'edit';
        $edit_attributes['href'] = Url::fromRoute(
          'entity.entity_abuse_report.edit_form',
          ['entity_abuse_report' => $existing_report->id()],
          ['query' => ['destination' => $destination]]
        )->toString();

        $build['#update'] = TRUE;
        $build['#update_label'] = $this->entityAbuseService->getEditReportLinkLabel();
        $build['#update_attributes'] = new Attribute($edit_attributes);
      }

      if ($this->currentUser->hasPermission('delete own entity_abuse_report')) {
        $delete_attributes = $attributes;
        $delete_attributes['class'][] = 'cancel';
        $delete_attributes['href'] = Url::fromRoute(
          'entity.entity_abuse_report.delete_form',
          ['entity_abuse_report' => $existing_report->id()],
          ['query' => ['destination' => $destination]]
        )->toString();

        $build['#cancel'] = TRUE;
        $build['#cancel_label'] = $this->entityAbuseService->getCancelReportLinkLabel();
        $build['#cancel_attributes'] = new Attribute($delete_attributes);
      }
    }
    else {
      $attributes['class'][] = 'create';
      $attributes['href'] = Url::fromRoute(
        'entity.entity_abuse_report.add_form',
        ['entity_type' => $entity_type, 'entity_id' => $entity_id],
        ['query' => ['destination' => $destination]]
      )->toString();

      $build['#create'] = TRUE;
      $build['#create_label'] = $this->entityAbuseService->getAddReportLinkLabel();
      $build['#create_attributes'] = new Attribute($attributes);
    }

    return $build;
  }

}
