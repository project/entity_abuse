<?php

namespace Drupal\entity_abuse\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\entity_abuse\EntityAbuseServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The main controller for the module.
 */
class EntityAbuseNoAccess extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity abuse service.
   *
   * @var \Drupal\entity_abuse\EntityAbuseServiceInterface
   */
  protected $entityAbuseService;

  /**
   * Constructs a new object.
   *
   * @param \Drupal\entity_abuse\EntityAbuseServiceInterface $entity_abuse_service
   *   The entity abuse service.
   */
  public function __construct(EntityAbuseServiceInterface $entity_abuse_service) {
    $this->entityAbuseService = $entity_abuse_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_abuse.service')
    );
  }

  /**
   * Callback to handle user's Email verification.
   *
   * @param string $entity_type
   *   Type ID of an entity to build report for.
   * @param int $entity_id
   *   ID of an entity to build report for.
   */
  public function page($entity_type, $entity_id) {

    // @todo Add logging of $entity_type & $entity_id data for statistics?
    $message = $this->entityAbuseService->getNoAccessMessage();

    return [
      '#type' => 'processed_text',
      '#text' => $message['value'],
      '#format' => $message['format'],
    ];
  }

}
