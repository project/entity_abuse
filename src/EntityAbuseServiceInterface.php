<?php

namespace Drupal\entity_abuse;

use Drupal\user\UserInterface;

/**
 * Interface defining Entity abuse handling helper service.
 */
interface EntityAbuseServiceInterface {

  /**
   * Return a list of entity type IDs + bundles with enabled complain ability.
   *
   * @return array
   *   List of entity type IDs + bundles with enabled complain ability.
   */
  public function getEnabledEntityTypes();

  /**
   * Check is report link display mode a dialog (modal or usual).
   *
   * @return bool
   *   Is report link display mode a dialog (modal or usual).
   */
  public function isReportLinkDialog();

  /**
   * Return report link behavior.
   *
   * @return string
   *   Report link behavior.
   */
  public function getReportLinkBehavior();

  /**
   * Return user cancel method.
   *
   * @return string
   *   User cancel method.
   */
  public function getUserCancelMethod();

  /**
   * Return no access behavior key.
   *
   * @return string
   *   No access behavior key.
   */
  public function getNoAccessBehavior();

  /**
   * Return Add report link label.
   *
   * @return string
   *   Add report link label.
   */
  public function getAddReportLinkLabel();

  /**
   * Return message for report was added event.
   *
   * @return array
   *   Message (formatted text) for report was added event.
   */
  public function getReportAddedMessage();

  /**
   * Return message for no access event.
   *
   * @return array
   *   Message (formatted text) for no access event.
   */
  public function getNoAccessMessage();

  /**
   * Return Edit report link label.
   *
   * @return string
   *   Edit report link label.
   */
  public function getEditReportLinkLabel();

  /**
   * Return message for report was updated event.
   *
   * @return array
   *   Message (formatted text) for report was updated event.
   */
  public function getReportEditedMessage();

  /**
   * Return Cancel report link label.
   *
   * @return string
   *   Cancel report link label.
   */
  public function getCancelReportLinkLabel();

  /**
   * Return message for report was canceled event.
   *
   * @return array
   *   Message (formatted text) for report was canceled event.
   */
  public function getReportCanceledMessage();

  /**
   * Return message for report cancel page (or popup).
   *
   * @return array
   *   Message (formatted text) for report cancel page (or popup).
   */
  public function getCancelReportNotification();

  /**
   * Return existing report for currently logged user.
   *
   * @param int $entity_id
   *   ID of an entity to look report for.
   * @param string $entity_type
   *   Type ID of an entity to look report for.
   *
   * @return \Drupal\entity_abuse\EntityAbuseReportInterface|null
   *   Existing report for currently logged user on success or NULL otherwise.
   */
  public function getExistingReport($entity_id, $entity_type);

  /**
   * Manage user reports on user delete action.
   *
   * @param \Drupal\user\UserInterface $user
   *   User to be deleted.
   */
  public function userDelete(UserInterface $user);

}
