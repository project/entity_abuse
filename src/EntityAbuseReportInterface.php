<?php

namespace Drupal\entity_abuse;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a "Entity abuse report" entity.
 */
interface EntityAbuseReportInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

}
