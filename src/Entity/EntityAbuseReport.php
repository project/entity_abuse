<?php

namespace Drupal\entity_abuse\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity_abuse\EntityAbuseReportInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the "Entity abuse report" entity.
 *
 * @ContentEntityType(
 *   id = "entity_abuse_report",
 *   label = @Translation("Abuse report"),
 *   label_collection = @Translation("Abuse reports"),
 *   label_singular = @Translation("abuse report"),
 *   label_plural = @Translation("abuse reports"),
 *   label_count = @PluralTranslation(
 *     singular = "@count abuse report",
 *     plural = "@count abuse reports",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "storage_schema" = "Drupal\entity_abuse\EntityAbuseReportStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "access" = "Drupal\entity_abuse\EntityAbuseReportAccessControlHandler",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\entity_abuse\Form\EntityAbuseReportForm",
 *       "delete" = "Drupal\entity_abuse\Form\EntityAbuseReportDeleteForm"
 *     },
 *   },
 *   base_table = "entity_abuse_report",
 *   uri_callback = "entity_abuse_report_uri",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "owner" = "uid",
 *   },
 *   field_ui_base_route = "entity_abuse.settings",
 *   links = {
 *     "canonical" = "/entity-abuse-report/{entity_abuse_report}",
 *     "delete-form" = "/entity-abuse-report/{entity_abuse_report}/delete",
 *     "edit-form" = "/entity-abuse-report/{entity_abuse_report}/edit",
 *     "create" = "/entity-abuse-report",
 *   },
 * )
 */
class EntityAbuseReport extends ContentEntityBase implements EntityAbuseReportInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['id']->setLabel(t('Abuse report ID'))
      ->setDescription(t('Identifier of an abuse report.'));

    $fields['uuid']->setDescription(t('Abuse report UUID.'));
    $fields['langcode']->setDescription(t('Abuse report language code.'));

    $fields['uid']
      ->setLabel(t('Reported by'))
      ->setDescription(t('Reference to the abuse report author.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Reported on'))
      ->setDescription(t('The time that the abuse report was created.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed on'))
      ->setDescription(t('The time that the abuse report was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Entity ID'))
      ->setDescription(new TranslatableMarkup('ID of an entity which was reported as abusive.'))
      ->setSetting('unsigned', TRUE);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Entity type'))
      ->setDescription(new TranslatableMarkup('Type of an entity which was reported as abusive.'))
      ->setSetting('max_length', 64);

    return $fields;
  }

}
