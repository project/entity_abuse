<?php

/**
 * @file
 * Contains integration with Views module.
 */

/**
 * Implements hook_views_data_alter().
 */
function entity_abuse_views_data_alter(&$data) {

  $data['entity_abuse_report']['entity_abuse_report_entity'] = [
    'title' => t('Related entity'),
    'field' => [
      'title' => t('Related entity'),
      'help' => t('A link to the entity which was reported as abusive.'),
      'id' => 'entity_abuse_report_entity',
    ],
  ];
}
